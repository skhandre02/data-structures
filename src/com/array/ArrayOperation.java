package com.array;

import java.util.Arrays;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class ArrayOperation<T> {

    private Object array[];

    public ArrayOperation(T []array) {
        this.array = array;
    }

    public ArrayOperation(int size) {
        this.array = new Object[size];
    }

    public T[] getArray() {
        return (T[])this.array;
    }

    public Stream stream() {
        return Arrays.stream(this.array);
    }

    @Override
    public String toString() {
        return "ArrayOperation{" +
                "array=" + Arrays.toString(array) +
                '}';
    }

    public void insertElementAtParticularIndex(T item, int index) {
        final int updatedSize = this.array.length+1;
        final ArrayOperation<T> arrayOperation = new ArrayOperation<>(updatedSize);
        final T[] updatedArray = arrayOperation.getArray();
        final AtomicInteger atomicInteger = new AtomicInteger(0);
        IntStream.range(0,this.array.length).forEach(i->{
            if (i==index) {
                updatedArray[atomicInteger.getAndIncrement()] = item;
           }
            updatedArray[atomicInteger.getAndIncrement()] = (T)this.array[i];
        });
        this.array = updatedArray;
    }
    public T get(int index) throws ArrayIndexOutOfBoundsException {
        if (this.array.length<=index) {
            throw new ArrayIndexOutOfBoundsException();
        } else {
            return (T)this.array[index];
        }
    }

    public int indexOf(T item) {
        for (int index=0;index<this.array.length;index++) {
            if (this.array[index] == item) {
                return index;
            }
        }
        return -1;
    }

    public boolean contains(T item) {
        for (int index =0 ;index<this.array.length;index++) {
            if (this.array[index] == item) {
                return true;
            }
        }
        return false;
    }


    public static void main(String[] args) {
        ArrayOperation<Integer> arrayOperation = new ArrayOperation<>(new Integer[] {10,20,30,40,1,20});
        System.out.println(arrayOperation);
        arrayOperation.insertElementAtParticularIndex(60,3);
        System.out.println(arrayOperation);
        System.out.println("contains: 30 - "+arrayOperation.contains(30));
        System.out.println(arrayOperation.indexOf(100));
    }
}
