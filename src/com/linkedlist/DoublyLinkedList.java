package com.linkedlist;

public class DoublyLinkedList<T> {

    private Node<T> head;

    private Node<T> tail;

    private static class Node<T> {
        T data;
        Node<T> next;
        Node<T> previous;

        Node(T data) {
            this.data = data;
        }
    }

    public DoublyLinkedList(T data) {
        final Node<T> node = new Node<T> (data);
        this.head = node;
        this.tail = node;
    }

    public void add(T data) {
        final Node<T> node = new Node<T> (data);
        node.previous = tail;
        tail.next = node;
        tail = node;
    }

    public boolean remove(T data) {
        Node iteratingNode = head;
        while(iteratingNode!=null) {
            if (iteratingNode.data == data) {
                if (head.data == data) {
                    head = head.next;
                }
                if (tail.data == data) {
                    tail = tail.previous;
                }
                iteratingNode.previous.next = iteratingNode.next;
                iteratingNode.next.previous = iteratingNode.previous;
                return true;
            }
            iteratingNode = iteratingNode.next;
        }
        return false;
    }

    public void print(final boolean forward) {
        if (forward) {
            Node iteratingNode = head;
            while(iteratingNode!=null) {
                System.out.print(iteratingNode.data);
                if (iteratingNode.next!=null)
                    System.out.print(" -> ");
                iteratingNode = iteratingNode.next;
            }
        } else {
            Node iteratingNode = tail;
            while(iteratingNode!=null) {
                System.out.print(iteratingNode.data);
                if (iteratingNode.previous!=null)
                    System.out.print(" -> ");
                iteratingNode = iteratingNode.previous;
            }
        }
        System.out.println("\n");
    }

    public static void main(String[] args) {
        DoublyLinkedList<Integer> doublyLinkedList = new DoublyLinkedList<>(10);
        doublyLinkedList.add(20);
        doublyLinkedList.add(30);
        doublyLinkedList.add(40);
        doublyLinkedList.add(50);
        System.out.println("Print Forward Direction: ");
        doublyLinkedList.print(true);
        System.out.println("Print Reverse Direction: ");
        doublyLinkedList.print(false);
        System.out.println("Remove 30: "+doublyLinkedList.remove(30));
        System.out.println("Print Forward Direction: ");
        doublyLinkedList.print(true);
        System.out.println("Print Reverse Direction: ");
        doublyLinkedList.print(false);

    }
}
