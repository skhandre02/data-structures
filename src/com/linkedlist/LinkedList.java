package com.linkedlist;

public class LinkedList<T> {
    private Node<T> head;
    private Node<T> tail;

    private static class Node<T> {
        T data;
        Node<T> next;
        Node (T data) {
            this.data = data;
        }
    }
    public LinkedList(T data) {
        final Node<T> node = new Node<T>(data);
        this.head = node;
        this.tail = node;
    }

    public void add(T data) {
        final Node<T> node = new Node<T>(data);
        this.tail.next = node;
        this.tail = node;
    }

    public T head() {
        return head!=null?head.data:null;
    }

    public T tail() {
        return tail!=null?tail.data:null;
    }

    public boolean remove(T data) {
        Node iteratingNode = this.head;
        Node previousNode = iteratingNode;
        while(iteratingNode!=null) {
            if (iteratingNode.data == data) {
                if (tail.data == data) {
                    tail = previousNode;
                }
                if (head.data == data) {
                    head = head.next;
                }
                previousNode.next = iteratingNode.next;
                return true;
            }
            previousNode = iteratingNode;
            iteratingNode = iteratingNode.next;
        }
        return false;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        Node iteratingNode = this.head;
        stringBuilder.append("[");
        while(iteratingNode!=null) {
            stringBuilder.append(iteratingNode.data);
            if (iteratingNode.next!=null)
                stringBuilder.append(" -> ");
            iteratingNode = iteratingNode.next;
        }
        stringBuilder.append("]");
        return stringBuilder.toString();
    }


    public static void main(String[] args) {
        LinkedList<Integer> list = new LinkedList<>(20);
        list.add(30);
        list.add(40);
        System.out.println(list.head());
        System.out.println(list.tail());
        System.out.println(list.toString());
        System.out.println(list.head());
        System.out.println(list.tail());
        System.out.println(list.remove(20));
        System.out.println(list.toString());
    }
}
