package com.stack;

import java.util.Arrays;

public class Stack<T> {
    private static int top;
    private T[] array;
    private static int capacity = 5;

    public Stack() {
        this.top = -1;
        this.array =(T[]) new Object[capacity];
    }

    public Stack(int capacity) {
        this.top = -1;
        this.capacity = capacity;
        this.array = (T[]) new Object[capacity];
    }

    public void push(T data) {
        if (this.array.length == capacity) {
            this.updateCapacity();
        }
        this.array[++top] = data;
    }

    public T pop() {
        if (top!=-1) {
            T data = this.array[top--];
            return data;
        } else {
            return null;
        }
    }

    private void updateCapacity() {
        this.capacity = this.capacity * 2;
        T[] updatedArray =  (T[])new Object[capacity];
        for(int i =0 ;i<this.array.length;i++) {
            updatedArray[i] = this.array[i];
        }
        this.array = updatedArray;
    }


    @Override
    public String toString() {
        return "Stack{" +
                "array=" + Arrays.toString(Arrays.copyOf(array, top+1)) +
                '}';
    }

    public static void main(String[] args) {
        Stack<Integer> stack = new Stack<>();
        stack.push(10);
        stack.push(20);
        stack.push(30);
        stack.push(40);
        stack.push(50);
        stack.push(60);
        stack.push(70);
        System.out.println(stack);
        System.out.println("pop operation: "+ stack.pop());
        System.out.println(stack);
    }
}
